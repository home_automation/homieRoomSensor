#include <Homie.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

#define SEALEVELPRESSURE_HPA (1013.25)

const int TEMPERATURE_INTERVAL = 300;  // Interval in s

unsigned long lastTemperatureSent = 0;

HomieNode balcClima("balc", "Balcony", "clima");
Adafruit_BME280 bme; // I2C

void setupSerial() {
  Serial.begin(115200);
  Serial << endl << endl;
}

void loopHandler() {
  if (millis() - lastTemperatureSent >= TEMPERATURE_INTERVAL * 1000UL || lastTemperatureSent == 0) {
    float temp = bme.readTemperature();
    float press = bme.readPressure() / 100.0F;
    float humid = bme.readHumidity();
    Homie.getLogger() << "Temperature: " << temp << " °C" << endl;
    Homie.getLogger() << "Pressure: " << press << " hPa" << endl;
    Homie.getLogger() << "Humidity: " << humid << " %" << endl;
    balcClima.setProperty("temp").send(String(temp));
    balcClima.setProperty("press").send(String(press));
    balcClima.setProperty("humid").send(String(humid));
    lastTemperatureSent = millis();
  }
}

void setupMQTT() {
  Homie_setFirmware("mqttSensor", "0.0.3");
  Homie.setLoopFunction(loopHandler);

  balcClima.advertise("temp")
    .setName("Temperature")
    .setDatatype("float")
    .setUnit("ºC");
  balcClima.advertise("press")
    .setName("Atmospheric pressure")
    .setDatatype("float")
    .setUnit("hPa");
  balcClima.advertise("humid")
    .setName("Humidity")
    .setDatatype("float")
    .setUnit("%");

  Homie.setup();
}

void setupSensor() {
  Serial.println("Init sensor.");

  if (!bme.begin(0x76, &Wire)) {
      Serial.println("Could not find a valid BME280 sensor, check wiring!");
      while (1);
  }

  Serial.println("Initialized sensor.");
}

void setup() {
  setupSerial();
  setupMQTT();
  setupSensor();
}

void loop() {
  Homie.loop();
}
